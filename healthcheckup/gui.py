# Dash framework
# https://dash.plotly.com/introduction
from dash import Dash, dcc, html, Input, Output, State, dash_table
import pandas as pd
import os

# Dash bootstrap librairy
# https://dash-bootstrap-components.opensource.faculty.ai/docs/components/button/
import dash_bootstrap_components as dbc
# regex
import re

# import search_engine
import search_engine as se


"""This file is the gui of the project

All the the function annotate with @app.callback are executed on the server side
The app is state less, only the browser store some cache in the memory to avoid unnecessary calls to the server


"""


options = se.searchSingleSymptoms()

# Create a Dash app instance
app = Dash(__name__, external_stylesheets=[dbc.themes.LITERA])
app.title = 'Healthcheckup'

navbar = dbc.NavbarSimple(
    brand="Health checker by Mair Marco & Cabannes Gwenael ",
    brand_href="#",
    color="rgb(21 40 39)",
    dark=True,
    className='navbar ',
)


accordion1 = dbc.AccordionItem(
    [
        dbc.Row([
                html.Div([
                    "Select your symptoms",
                    dcc.Dropdown(options['Symptom Name'], id="symptoms-dynamic-dropdown", multi=True)]),
                ]),
        html.Br(),
        html.Div([dash_table.DataTable([], id='table-mulitple-symptoms',
                                       style_header={'visibility': 'none'},
                                       style_cell={'visibility': 'none'},
                                       )], id='div-mulitple-symptoms')
    ], title="🩺 1. Type your symptoms", id='accordion1'
)

accordion2 = dbc.AccordionItem(
    [
        html.Div([dash_table.DataTable([], id='table-conditions',
                                       style_header={'visibility': 'none'},
                                       style_cell={'visibility': 'none'},
                                       ),
                  html.Br(),
                  ], id='div-conditions_for_sympoms'),
        html.Div([
            dcc.Loading(
                id="loading-2",
                children=[html.Div([html.Div(id="loading-output-2")])],
                type="default", style={'width': '20%', 'height': '20%'}
            ),
            html.Div(id='URL-condition'),
            html.Iframe(
                sandbox='',
                srcDoc=''' ''',
                style={'font-family': '"Segoe UI"', "height": "1000px", "width": "50%"}, id='iframe-condition-detail'),
            html.Iframe(
                sandbox='',
                srcDoc=''' ''',
                style={'font-family': '"Segoe UI"', "height": "1000px", "width": "50%"}, id='iframe-treatment-option')],
            className='row-container'),
    ], title="📄 2. Explore possible conditions", id='accordion2'
)


app.layout = html.Div([
    navbar,
    html.Div([
        # The memory store reverts to the default on every page refresh
        dcc.Store(id='memstore'),
        html.Div([
            html.H4('How to user Health checker'),
            html.Ol([
                html.Li(["Add your symptoms. Results appear."]),
                html.Li(
                    'Click on the list of symptoms that match the best. A list condition appears'),
                html.Li('Click on a condition to see the details'),
            ]),
        ]),
        dbc.Accordion(
            [accordion1, accordion2
             ], flush=True, always_open=True, id='accordions'
        ),
        html.Br(),
        dbc.Button(id="reset-button", color="secondary", outline=True,
                   className="me-1", n_clicks=0, children='Reset'),
    ], style={'margin': 50, 'margin-top': 20, 'min-height': '100%'}),
    html.Footer(
        html.Div([html.P(['Health checker does not replace the diagnostic of a doctor. All data are extrated from ',
                          html.A(['https://symptomchecker.webmd.com'],
                                 href='https://symptomchecker.webmd.com',
                                 target="_blank")])],
                 style={'padding': 10, 'text-align': 'center', 'backgroundColor': '#f8f9fa'}))
], style={
    'display': 'grid',
    'grid-template-rows': 'auto 1fr auto',
    'min-height': '100vh'})


@ app.callback(
    Output("symptoms-dynamic-dropdown", "value"),
    Input("reset-button", "n_clicks"),
    State("symptoms-dynamic-dropdown", "value")
)
def reset_symptoms_value(search_value, value):
    # print('reset_symptoms_value callback')
    return []


######################################################################################################


@ app.callback(
    Output('div-mulitple-symptoms', 'children'),
    Input("symptoms-dynamic-dropdown", "value"),
    State("symptoms-dynamic-dropdown", "value"),
)
def display_table_mulitple_symptoms(value, data):
    hidden_table = dash_table.DataTable([], id='table-mulitple-symptoms',
                                        style_header={
        'visibility': 'none'},
        style_cell={'visibility': 'none'},
    )
    if data is not None and len(data) > 0:
        print("Call searchMulitpleSymptoms")
        df = se.searchMulitpleSymptoms(data)
        if len(df.index) == 0:
            return html.Div(['No result fround, try to remove symptoms', hidden_table])

        # convert data into a string used as the id of the rows of the table
        df['id'] = df[['Symptom Name', 'Symptom URL']].values.tolist()
        df['id'] = df['id'].astype(str)
        return [str(len(df.index)) + ' results found. '
                "Click on the line which match the best with your health status",
                dash_table.DataTable(df.to_dict('records'),
                                     columns=[
                                         {"name": 'Symptoms', "id": 'Symptom Name'}],
                                     style_cell={'textAlign': 'left'},
                                     style_header={'textAlign': 'center'},
                                     style_data={
                                         'whiteSpace': 'normal'},
                                     page_size=10,
                                     id='table-mulitple-symptoms',
                                     style_data_conditional=[
                    {"if": {"state": "selected"},
                        "backgroundColor": "inherit !important", }
                ])]
    return hidden_table


######################################################################################################


@ app.callback([Output('div-conditions_for_sympoms', 'children'),
                Output('memstore', 'data'),
                Output('accordion2', 'class_name'),
                Output('accordions', 'active_item')],
               [Input('table-mulitple-symptoms', 'active_cell'),
               State('memstore', 'data')])
def display_conditions_for_symptoms(rows, data):
    hidden_table = dash_table.DataTable([], id='table-conditions',
                                        style_header={
        'visibility': 'none'},
        style_cell={'visibility': 'none'},
    )

    if rows is None:
        return hidden_table, data, "disabled", ['item-0']

    # get data from string to a dataframe
    rows = eval(rows['row_id'])
    d = {'Symptom Name': rows[0:-1],
         'Symptom URL': rows[-1]}
    df = pd.DataFrame(data=d,  index=[0])
    print("Call getConditionsForSymptom")
    conditions = se.getConditionsForSymptom(df)

    if len(conditions.index) == 0:
        return html.Div(['No result fround, try to choose othe symptoms', hidden_table]), data, "", ['item-0', 'item-1']

    data = {} if data is None else data
    data['conditions_for_symptoms'] = conditions.to_json()

    table_df = conditions.copy()
    table_df['id'] = conditions['Condition Name'].astype(str)

    output_and_datatable = [str(len(table_df.index)) + ' conditions found. '
                            "Click on a line to see more details",
                            dash_table.DataTable(table_df.to_dict('records'),
                                                 [{"name": 'Conditions', "id": 'Condition Name'},
                                                  {"name": 'Description', "id": 'Condition Description'}],
                                                 style_cell={
                                                     'textAlign': 'left'},
                                                 style_header={
                                                     'textAlign': 'center'},
                                                 style_data={
                                                     'whiteSpace': 'normal'},
                                                 page_size=10,
                                                 id='table-conditions',
                                                 style_data_conditional=[
                                {"if": {"state": "selected"},
                                 "backgroundColor": "inherit !important", }
                            ])]
    return output_and_datatable, data, "", ['item-0', 'item-1']


######################################################################################################


@ app.callback([Output("loading-output-2", "children"),
                Output('iframe-condition-detail', 'srcDoc'),
                Output('iframe-treatment-option', 'srcDoc'),
                Output('URL-condition', 'children')],
               [Input('table-conditions', 'active_cell'),
               State('memstore', 'data')])
def display_condition_details(rows, data):

    if rows is None:
        return "", "", "", ""

    css = str('''<style> html * {font-family: 'Segoe UI' !important;}</style>''')
    condition_detail = ''''''
    treatment_option = ''''''

    if rows is None:
        return "", condition_detail, treatment_option, ""

    conditions = pd.read_json(data['conditions_for_symptoms'])
    # print(conditions)
    condition = conditions[conditions['Condition Name'] == rows['row_id']]
    print("Call getConditionDetails")
    condition_detail, treatment_option = se.getConditionDetails(
        condition)
    url_condition = [html.Span(['Find the original page ', html.A(
        'Here', href=condition['Condition URL'].to_numpy()[0], target="_blank"), ])]

    return "", css + str(condition_detail), css + str(treatment_option), url_condition


######################################################################################################

def run():
    app.run_server(host='0.0.0.0', port=8050, debug=True)


# @ app.server.route('/shutdown', methods=['POST'])
# def shutdown():
#     shutdown_server()
#     return 'Server shutting down..'


if __name__ == '__main__':
    run()
