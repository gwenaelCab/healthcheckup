# Import own scripts
from .. import search_engine as se

# Import necessary libraries
import unittest
import pandas as pd
import os
import pytest

# get current path
__location__ = os.path.realpath(os.path.join(
    os.getcwd(), os.path.dirname(__file__)))  # relative filepath


# Create testclass
class TestSearchEngine(unittest.TestCase):
    def test_searchSingleSymptoms(self):
        """
        Test searchSingleSymptoms if it contains anything
        """
        # Create data output that should be generated
        df = pd.read_csv(__location__ + "\\unit\\fixtures\\test_search_engine\\searchSingleSymptoms.csv", index_col=0)

        # Get results from function
        result = se.searchSingleSymptoms()

        #if(result == df):
        #    print("FUCK TESTING")

        #self.assertEqual(len(df.index), len(result.index))
        self.assertEqual(True, df.equals(result))

    def test_searchMulitpleSymptoms(self):
        """
        Test searchMulitpleSymptoms 
        """
        # Create data output that should be generated

        df = pd.read_csv(
            __location__ + "\\unit\\fixtures\\test_search_engine\\searchMultipleSymptomsResult.csv", index_col=0)

        # Get results from function
        result = se.searchMulitpleSymptoms(["Dry skin", "Dizziness", "Chills"])

        self.assertEqual(True, df.equals(result))

    def test_getConditionsForSymptom(self):
        """
        Test getConditionsForSymptom 
        fails sometimes because site redirects differently with same link
        """
        # Create data output that should be generated
        df = pd.read_csv(
            __location__ + "\\unit\\fixtures\\test_search_engine\\getConditionsForSymptom.csv", index_col=0)

        # Create dataframe for input
        dataFrame = pd.DataFrame({"Symptom Name" : ["Dry skin, Dizziness, Chills"],
                                  "Symptom URL" : ["https://symptomchecker.webmd.com/multiple-symptoms?symptoms=binge-eating|chills|dizziness|dry-skin&symptomids=310|51|81|86&locations=66|66|66|66"]})

        # Get results from function
        result = se.getConditionsForSymptom(dataFrame)

        self.assertEqual(True, df.equals(result))

    def test_getConditionDetails(self):
        """
        Test getConditionDetails 
        """

        # Create data output that should be generated, because this function returns  dataframes
        f1 = open(
            __location__ + "\\unit\\fixtures\\test_search_engine\\getConditionDetailsConditionDetails.txt", "r")
        input_txt1 = f1.read()
        f2 = open(
            __location__ + "\\unit\\fixtures\\test_search_engine\\getConditionDetailsTreatmentOptions.txt", "r")
        input_txt2 = f2.read()


        # Create dataframe for input
        input_dataframe = pd.DataFrame({"Condition Name": ["Influenza"],
                                        "Condition Description": ["The flu is a respiratory tract infection and causes fever, sore throat, runny nose, headache, cough, and more."],
                                        "Condition URL": ["https://symptoms.webmd.com/symptomcheckercondition?condition=091e9c5e808e81a5&bpid%5B0%5D=2&sid%5B0%5D=102"]})

        # Get results from function
        result1, result2 = se.getConditionDetails(input_dataframe)

        self.assertEqual(str(result1), input_txt1)
        self.assertEqual(str(result2), input_txt2)

    def test_remove_html_tags(self):
        """
        Test remove_html_tags
        """

        data = "What To ExpectOver time, bulimia can cause many health problems, including cavities, constipation, hemorrhoids, pancreatitis, miscarriage, and dehydration. Treatment usually involves therapy, support groups, and antidepressant medications."

        f = open(
            __location__ + "\\unit\\fixtures\\test_search_engine\\remove_html_tags.txt", "r")
        input_txt = f.read()

        # Get results from function
        result = se.remove_html_tags(input_txt)

        self.assertEqual(result, data)


if __name__ == '__main__':
    unittest.main()
