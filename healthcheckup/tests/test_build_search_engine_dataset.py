# Import own scripts
from .. import build_search_engine_dataset as bsed

# Import necessary libraries
import unittest
import pandas as pd
import os
import pytest

# get current path
__location__ = os.path.realpath(os.path.join(
    os.getcwd(), os.path.dirname(__file__)))  # relative filepath


# Information:
# It is not practical to test all functions of build_search_engine_dataset because it takes up tp 2 hours for this dataset to be build.
# Furthermore it is possible that the functions lose connection while retrieving data and then I restarted them manually with a differential
# index to be able to retrieve the missing data parts. But therefore it is not practical to test this behaviour.
# The script was mainly used for building the first dataset and I will also provide tests for functions that are not used to get the whole set.


# Create testclass
@pytest.mark.skip(reason="Currently takes up to 2 hours")
class TestBuildSearchEngineDataset(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        bsed.buildDataSet()

    def test_buildSingleSymptomsCSV(self):
        """
        Test buildSingleSymptomsCSV 
        """

        # Get data output that should be generated
        df = pd.read_csv(
            __location__ + "\\unit\\fixtures\\test_build_search_engine_dataset\\buildSingleSymtpomsCSV.csv", index_col=0)

        # Get results from function
        result = bsed.buildSingleSymptomsCSV()

        self.assertEqual(True, df.equals(result))

    # Function that uses prebuild dataset

    def test_buildMergedSymptomDataCSV(self):
        """
        Test buildMergedSymptomDataCSV 
        """
        # Get data output that should be generated
        df = pd.read_csv(
            __location__ + "\\unit\\fixtures\\test_build_search_engine_dataset\\buildMergedSymptomDataCSV.csv", index_col=0)

        # Get results from function
        result = bsed.buildMergedSymptomDataCSV()

        self.assertEqual(True, df.equals(result))

    # Function that uses prebuild dataset

    def cleanCSVFromDuplicates(self):
        """
        Test buildMergedSymptomDataCSV 
        """
        # Get data output that should be generated
        df = pd.read_csv(
            __location__ + "\\unit\\fixtures\\test_build_search_engine_dataset\\cleanCSVFromDuplicates.csv", index_col=0)

        # Get results from function
        result = bsed.buildMergedSymptomDataCSV()

        self.assertEqual(True, df.equals(result))

    # Prepared test but due to changes display changes on the website it switches some entries in the output while the whole content stays the same so some rows
    # are not equal, therefore test would fail.
    # def test_getMulitpleSymptomsFromWEBMDSymptomsAZ(self):
    #    """
    #    Test getMulitpleSymptomsFromWEBMDSymptomsAZ
    #    """
    #    #Example
    #    input_symptom_name = "Fever"
    #    input_url = "https://symptomchecker.webmd.com/single-symptom?symptom=fever&symid=102&loc=2"
    #
    #    #Get data output that should be generated
    #    df = pd.read_csv(__location__ + "\\unit\\fixtures\\test_build_search_engine_dataset\\buildSingleSymtpomsCSV.csv", index_col=0)
    #
    #    #Get results from function
    #    result = bsed.getMultipleSymptomsFromWEBMDSymptomsAZ(input_symptom_name, input_url)
    #
    #    self.assertEqual(True, df.equals(result))


if __name__ == '__main__':
    unittest.main()
