import requests
import dash
from dash import html
import pytest
import os
from dash.testing.application_runners import import_app

# import_app doesn't support relative path so this file can't be in the test folder


def test_gui_can_start(dash_duo):
    app = import_app(app_file="gui")
    dash_duo.start_server(app)
    assert dash_duo.driver.title == "Healthcheckup"
