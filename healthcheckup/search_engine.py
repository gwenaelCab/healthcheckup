# Import libraries
import pandas as pd
import requests
import os
from bs4 import BeautifulSoup
from selenium import webdriver
# for implicit and explict waits
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options  # for suppressing the browser
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import re
import time


# predevined parameters:
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))  # relative filepath


########################################################
### FUNCTIONS USED FOR DATA ACCESS FROM GUI          ###
########################################################
def searchSingleSymptoms():
    """Provide SingleSymptomsDF data 
    :return: DataFrame with all symptoms
    """
    # Read .csv file as dataframe
    df = pd.read_csv(f"{__location__}/search_engine_data/SingleSymptomsDF.csv", index_col=0)

    # Search dataframe for input_text and resetting index
    #contain_values = df[df["Symptom Name"].str.contains(input_text)].reset_index(drop=True)

    # print(contain_values)

    return df


def searchMulitpleSymptoms(input_list):
    """Search AllMultipleSymptomsDF data for matching symptoms in SymptomNames
    :param1: input_list; list of strings containing symptoms for searching
    :return: DataFrame with all MultipleSymptoms matching the list of symptoms of the input list
    """

    # Read .csv file as dataframe
    df = pd.read_csv(f"{__location__}/search_engine_data/AllMultipleSymptomsDF.csv", index_col=0)

    # Search for dataframe for all inputs, after every input search overwrite previous dataframe -> in the end there will be only matching entries left
    for input in input_list:
        df = df[df["Symptom Name"].str.contains(input, regex=False)].reset_index(drop=True)

    print(df)

    return df


def getConditionsForSymptom(input_symptom_dataframe=pd.DataFrame()):
    """Get conditions from HTML page of symptom provided in input_dataframe
    :param1: input_dataframe; 1 row containing "Symptom Name" and "Symptom URL"
    :return: DataFrame with all Conditions linked to the symptom provided as input. DF contains "Condition Name", "Condition Description" and " Condition URL".
    """
    # Get name and symptom from dataframe
    # first row and first column
    symptomName = input_symptom_dataframe.iloc[0, 0]
    # first row and second column
    symptomURL = input_symptom_dataframe.iloc[0, 1]

    # Build html content request
    html_content = requests.get(symptomURL).text

    # Parse the html content
    soup = BeautifulSoup(html_content, 'lxml')
    # print(soup.prettify()) #print data

    # Find table with a-z symptoms in html code
    related_conditions = soup.find(
        "div", attrs={"class": "related_conditions"})

    # Find all entries in this table
    related_conditions_data_name_URL = related_conditions.find_all("a")
    related_conditions_data_description = related_conditions.find_all("p")

    listOfRelatedConditionsNames = []
    listOfRelatedConditionsDescription = []
    listOfRelatedConditionsURLS = []

    # For each entry (condition) get name, description and related link
    for each_conditon in related_conditions_data_name_URL:
        name = each_conditon.text
        href = each_conditon.get("href")

        # Append entries to list
        listOfRelatedConditionsNames.append(name)
        listOfRelatedConditionsURLS.append(href)

    for each_conditon in related_conditions_data_description:
        description = each_conditon.text

        # Append entries to list
        listOfRelatedConditionsDescription.append(description)

    # Create dictionary for dataframe
    d = {"Condition Name": listOfRelatedConditionsNames,
         "Condition Description": listOfRelatedConditionsDescription,
         "Condition URL": listOfRelatedConditionsURLS}

    # Create Dataframe
    AllLinkedConditions = pd.DataFrame(d)

    #print(AllLinkedConditions)

    return AllLinkedConditions


def getConditionDetails(input_condition_dataframe=pd.DataFrame()):
    """Get ALL information provided by the condition page. (Condition Details, Treatment Options)
    :param1: input_condition_dataframe; 1 row containing "Condition Name", "Condition Description" and "Condition URL"
    :return1: HTML string with all condition details
    :return2: HTML string with all treatment options
    :time: ~7s
    """
    # Get name and condition from dataframe
    # first row and first column
    conditionName = input_condition_dataframe.iloc[0, 0]
    # first row and second column
    conditionDescription = input_condition_dataframe.iloc[0, 1]
    # first row and third column
    conditionURL = input_condition_dataframe.iloc[0, 2]

    # Use selenium to get source code
    option = webdriver.ChromeOptions()
    option.add_argument("headless")

    browser = webdriver.Chrome(options=option)

    browser.get(conditionURL)

    # Try except for button, if the button is not there, it is not critical because it only would reveal more part of the overview topic
    try:
        # Click read more button to reveal hidden content in "Overview"
        element = WebDriverWait(browser, 10).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".read-more")))
        # Execute click script because it is more stable then element.click()
        browser.execute_script("arguments[0].click();", element)
    except Exception as e:
        print(f"Func: getConditionDetails | Error: {e}")

    # Get source code of condition details page
    source_code_condition_details = browser.page_source

    try:
        # Click treatment options button to switch to Treatment options
        element = WebDriverWait(browser, 10).until(EC.element_to_be_clickable(
            (By.CSS_SELECTOR, ".nav-tab > ul:nth-child(1) > li:nth-child(2) > a:nth-child(1) > span:nth-child(1)")))
        # Execute click script because it is more stable then element.click()
        browser.execute_script("arguments[0].click();", element)
    except Exception as e:
        print(f"Func: getConditionDetails | Error: {e}")

    # Get source code of condition details page
    source_code_treatment_options = browser.page_source

    browser.close()

    # Parse html source code into BeautifulSoup for data retrieval
    soup_condition_details = BeautifulSoup(
        source_code_condition_details, features="lxml")
    soup_treatment_options = BeautifulSoup(
        source_code_treatment_options, features="lxml")

    # Get div with Condition details from HTML content
    condition_details = soup_condition_details.find(
        "div", attrs={"class": "condition-article"})
    treatment_options = soup_treatment_options.find(
        "div", attrs={"class": "condition-article"})

    # INFORMATION GATHERING FOR CONDITION DETAILS
    # Remove HTML content for condition details
    try:
        remove_in_condition_details = condition_details.find_all(
            "div", attrs={"class": "nav-tab"})
        for match in remove_in_condition_details:
            match.decompose()
    except Exception as e:
        print("Removal element \"nav-tab\" not found.")
    try:
        remove_in_condition_details = condition_details.find_all(
            "a", href=True)
        for match in remove_in_condition_details:
            match.decompose()
    except Exception as e:
        print("Removal element \"learnmore-link\" not found.")
    try:
        remove_in_condition_details = condition_details.find_all(
            "button", attrs={"class": "solid-button continue-button treatment-option-button"})
        for match in remove_in_condition_details:
            match.decompose()
    except Exception as e:
        print("Removal element \"solid-button continue-button treatment-option-button\" not found.")
    try:
        remove_in_condition_details = condition_details.find_all(
            "div", attrs={"class": "content-footer"})
        for match in remove_in_condition_details:
            match.decompose()
    except Exception as e:
        print("Removal element \"content-footer\" not found.")

    # INFORMATION GATHERING FOR treatment options
    # Remove HTML content for treatment options
    try:
        remove_in_treatment_options = treatment_options.find_all(
            "div", attrs={"class": "nav-tab"})
        for match in remove_in_treatment_options:
            match.decompose()
    except Exception as e:
        print("Removal element \"nav-tab\" not found.")
    try:
        remove_in_treatment_options = treatment_options.find_all(
            "a", href=True)
        for match in remove_in_treatment_options:
            match.decompose()
    except Exception as e:
        print("Removal element \"learnmore-link\" not found.")
    try:
        remove_in_treatment_options = treatment_options.find_all(
            "button", attrs={"class": "solid-button continue-button treatment-option-button"})
        for match in remove_in_treatment_options:
            match.decompose()
    except Exception as e:
        print("Removal element \"solid-button continue-button treatment-option-button\" not found.")
    try:
        remove_in_treatment_options = treatment_options.find_all(
            "div", attrs={"class": "common-text"})
        for match in remove_in_treatment_options:
            match.decompose()
    except Exception as e:
        print("Removal element \"common-text\" not found.")


    text_file = open(__location__ + "condition_details.txt", "w")
    n = text_file.write(str(condition_details))
    text_file.close()
    text_file2 = open(__location__ + "treatment_options.txt", "w")
    n = text_file2.write(str(treatment_options))
    text_file2.close()

    return condition_details, treatment_options


def remove_html_tags(text):
    """Remove html tags from a string
    :param: string that has html tags
    :return: string without html tags left
    """
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)


########################################################
### TESTING AREA                                     ###
########################################################
#start = time.time()
# searchSingleSymptoms("D")
#stop = time.time()
#print(f"searchSingleSymptoms: {stop-start}s")

# There are ~ 20 entries for : "Chills, Dizziness, Dry skin"
#start = time.time()
#print(searchMulitpleSymptoms(["Dry skin", "Dizziness", "Chills"]))
#stop = time.time()
#print(f"searchMulitpleSymptoms: {stop-start}s")
#
# There are 17 conditions retrieved
#dataFrame = pd.DataFrame({"Symptom Name" : ["Dry skin, Dizziness, Chills"],
#                          "Symptom URL" : ["https://symptomchecker.webmd.com/multiple-symptoms?symptoms=binge-eating|chills|dizziness|dry-skin&symptomids=310|51|81|86&locations=66|66|66|66"]})
#start = time.time()
#print(getConditionsForSymptom(dataFrame))
#stop = time.time()
#print(f"getConditionsForSymptom: {stop-start}s")
#
# Condition info
#def use_function_marco():  # with function the code is run when I import the file in the gui!
#input_dataframe = pd.DataFrame({"Condition Name": ["Influenza"],
#                                        "Condition Description": ["The flu is a respiratory tract infection and causes fever, sore throat, runny nose, headache, cough, and more."],
#                                        "Condition URL": ["https://symptoms.webmd.com/symptomcheckercondition?condition=091e9c5e808e81a5&bpid%5B0%5D=2&sid%5B0%5D=102"]})
#start = time.time()
#condition_details, treatment_options = getConditionDetails(input_dataframe)
#stop = time.time()
#print(f"getConditionInformation: {stop-start}s")
#print(condition_details)
#print(treatment_options)
#
#
# Test other condition infos
# dataFrame3 = pd.DataFrame({"Condition Name" : ["Gingivitis"],
#                          "Condition Description" : ["Bulimia is an eating disorder that involves binging on large quantities of food then purging it by vomiting."],
#                          "Condition URL" : ["https://symptoms.webmd.com/symptomcheckercondition?condition=091e9c5e808e8156&bpid%5B0%5D=7&sid%5B0%5D=16&bpid%5B1%5D=7&sid%5B1%5D=438"]})
# dataFrame4 = pd.DataFrame({"Condition Name" : ["Common Cold"],
#                          "Condition Description" : ["Bulimia is an eating disorder that involves binging on large quantities of food then purging it by vomiting."],
#                          "Condition URL" : ["https://symptoms.webmd.com/symptomcheckercondition?condition=091e9c5e808e80ac&bpid%5B0%5D=10&sid%5B0%5D=59&bpid%5B1%5D=2&sid%5B1%5D=102&bpid%5B2%5D=2&sid%5B2%5D=115&bpid%5B3%5D=10&sid%5B3%5D=120"]})
# dataFrame5 = pd.DataFrame({"Condition Name" : ["Medication Reaction or Side Effect"],
#                          "Condition Description" : ["Bulimia is an eating disorder that involves binging on large quantities of food then purging it by vomiting."],
#                          "Condition URL" : ["https://symptoms.webmd.com/symptomcheckercondition?condition=091e9c5e808e7bea&bpid%5B0%5D=7&sid%5B0%5D=16&bpid%5B1%5D=7&sid%5B1%5D=438"]})
#
#condition_details1,treatment_options1,condition1 = getConditionDetails(dataFrame2)
# print(condition_details)
# print(treatment_options)
# print(condition)
