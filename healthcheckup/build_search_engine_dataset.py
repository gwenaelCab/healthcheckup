# Import libraries
import pandas as pd
import requests
import time
import os
import glob
from bs4 import BeautifulSoup


# predevined parameters:
__location__ = os.path.realpath(os.path.join(
    os.getcwd(), os.path.dirname(__file__)))  # relative filepath

########################################################
### FUNCTIONS USED FOR BUILDING THE DATASET          ###
########################################################


def buildSingleSymptomsCSV():
    """Get .csv file from SingleSymptoms from HTML page with "Symptom Name" and "Symptom URL"
    :return: SingleSymptoms dataframe
    :time: 0.5s
    """
    # USED LINK: https://symptomchecker.webmd.com/symptoms-a-z

    # Base URL of href component
    baseurl = "https://symptomchecker.webmd.com/"

    # Build html content request
    html_content = requests.get(
        "https://symptomchecker.webmd.com/symptoms-a-z").text

    # Parse the html content
    soup = BeautifulSoup(html_content, 'lxml')
    # print(soup.prettify()) #print data

    # Find table with a-z symptoms in html code
    symptomsAZ = soup.find("div", attrs={"id": "list_az"})

    # Find all entries in this table
    symptomsAZ_data = symptomsAZ.find_all("a")

    listOfSingleSymptoms = []
    # For each entry (symptom) get name and related link
    for each_single_symptom in symptomsAZ_data:
        name = each_single_symptom.text
        href = each_single_symptom.get("href")
        # print(name)
        #print(baseurl + href)

        # Save data to dictionary, then to list
        d = {"Symptom Name": name,
             "Symptom URL": baseurl + href,
             }
        # Append dicts to list
        listOfSingleSymptoms.append(d)

    SingleSymptomsDF = pd.DataFrame(listOfSingleSymptoms)

    SingleSymptomsDF.to_csv(
        __location__ + "\\search_engine_data\\SingleSymptomsDF.csv")

    return SingleSymptomsDF


def getMultipleSymptomsFromWEBMDSymptomsAZ(input_symptom_name, input_url):
    """Get dataframe with Multiple symptoms (e.g. Fever (4349 results))
    :param1: name of the SingleSypmtom
    :param2: URL of the SingleSymptom
    :return: dataframe of MultipleSymptoms with name and related link
    :time: 1.16s
    """
    # Example
    #input_symptom_name = "fever"
    #input_url = "https://symptomchecker.webmd.com/single-symptom?symptom=fever&symid=102&loc=2"

    # Base URL of href component
    baseurl = "https://symptomchecker.webmd.com/"

    # Build html content request
    html_content = requests.get(input_url).text

    # Parse the html content
    soup = BeautifulSoup(html_content, 'lxml')

    # Check for page count
    pagination = soup.find("div", attrs={"class": "pagination"})
    pages = pagination.find_all("li")
    # Get page count from entries - 3 (-1 for prev; -1 for next)
    page_ctr = len(pages)-2

    listOfAllMultipleSymptomsOfSingleSymptom = []

    # Check if input symptom got a \ or /
    if "/" or "\\" in input_symptom_name:
        fix_name = input_symptom_name.split("/")
        input_symptom_name = ','.join(fix_name)

    # If is only one page, page ctr will be empty []
    if pages == []:
        # Just parse URL because there is only 1 page
        url = input_url + "&page=1"
        listOfAllMultipleSymptomsFromPage = getMultipleSymptomsFromOneSourcePageWEBMDSymptomsAZ(
            input_symptom_name, url)

        listOfAllMultipleSymptomsOfSingleSymptom.extend(
            listOfAllMultipleSymptomsFromPage)

        MultipleSymptomsDF = pd.DataFrame(
            listOfAllMultipleSymptomsOfSingleSymptom)

        MultipleSymptomsDF.to_csv(
            __location__ + "\\search_engine_data\\multiple_symptoms\\" + input_symptom_name + ".csv")

        return MultipleSymptomsDF

    else:
        for i in range(page_ctr):
            i = i+1  # there is no page 0

            # Create url for OneSourcePage
            url = input_url + "&page=" + str(i)

            listOfAllMultipleSymptomsFromPage = getMultipleSymptomsFromOneSourcePageWEBMDSymptomsAZ(
                input_symptom_name, url)

            listOfAllMultipleSymptomsOfSingleSymptom.extend(
                listOfAllMultipleSymptomsFromPage)

        MultipleSymptomsDF = pd.DataFrame(
            listOfAllMultipleSymptomsOfSingleSymptom)

        MultipleSymptomsDF.to_csv(
            __location__ + "\\search_engine_data\\multiple_symptoms\\" + input_symptom_name + ".csv")

        return MultipleSymptomsDF


def getMultipleSymptomsFromOneSourcePageWEBMDSymptomsAZ(input_symptom_name, input_url):
    """Get list with Multiple symptoms (e.g. One page of fever) not use this one as standalone
    :param1: name of symptom
    :param2: url of multiple symptoms page corresponding to name of symptom
    :return: list with names and urls
    :time: 
    """
    # Base URL of href component
    baseurl = "https://symptomchecker.webmd.com/"

    # Build html content request
    html_content = requests.get(input_url).text

    # Parse the html content
    soup = BeautifulSoup(html_content, 'lxml')

    listOfMultipleSymptoms = []

    # Get table with MultipleSymptoms from HTML content
    related_symptoms_table = soup.find(
        "table", attrs={"class": "results_table"})

    # Get data from <a content </a>
    related_symptoms_table_data = related_symptoms_table.find_all("a")

    # For each entry (symptom) get name and related link
    for each_multiple_symptom in related_symptoms_table_data:
        name = each_multiple_symptom.text
        href = each_multiple_symptom.get("href")

        # Save data to dictionary, then to list
        d = {"Symptom Name": name,
             "Symptom URL": baseurl + href,
             }
        # Append dicts to list
        listOfMultipleSymptoms.append(d)

    # Next page handling

    # Create dataframe from list
    MultipleSymptomsDF = pd.DataFrame(listOfMultipleSymptoms)

    # Check if input symptom got a \ or /
    if "/" or "\\" in input_symptom_name:
        fix_name = input_symptom_name.split("/")
        input_symptom_name = ','.join(fix_name)

    MultipleSymptomsDF.to_csv(__location__ + "\\search_engine_data\\multiple_symptoms\\multiple_symptoms_single_pages\\" +
                              input_symptom_name + "_page" + input_url[-1] + ".csv")

    return listOfMultipleSymptoms


def getAllMultipleSymptomsFromWEBMDSymptomsAZ(SingleSymptomsDF, start_index=0):
    """Get dataframe with ALL MultipleSymptoms of SingleSymptoms
    :param: SingleSymptomsDF; dataframe with list of names and links of SingleSymptoms
    :return: dataframe of ALL MultipleSymptoms with name and related link
    """
    # Get index length in pandas dataframe
    df_index_count = len(SingleSymptomsDF.index)

    # Dataframe Header: -> iloc access with [ROW, COLUMN]
    # "Symptom Name" | "URL"
    for i in range(df_index_count-start_index):
        print(
            f"Index: {i+start_index} | Symptom Name: {SingleSymptomsDF.iloc[i+start_index,0]} | URL: {SingleSymptomsDF.iloc[i+start_index,1]}")
        getMultipleSymptomsFromWEBMDSymptomsAZ(
            SingleSymptomsDF.iloc[i+start_index, 0], SingleSymptomsDF.iloc[i+start_index, 1])

        # needs to be set otherwise server will may disconnect the requests
        time.sleep(5)


def buildMultipleSymptomsCSV():
    """Get csv. data from ALL symptoms into seperate csv files
    Creates csv. data of 466 symptoms (e.g. //search_engine_data//multiple_symptoms//fever.csv)
    Use this methode only to update or build the data
    Can take up to 1 hour due to sleep timing otherwise requests get denied by the server
    """
    SingleSymptomsDF = buildSingleSymptomsCSV()
    getAllMultipleSymptomsFromWEBMDSymptomsAZ(SingleSymptomsDF)


def buildMergedSymptomDataCSV():
    """Get csv. data from ALL symptoms into merged file
    Merges csv. data of 466 symptoms (e.g. //search_engine_data//multiple_symptoms//fever.csv) together
    Will save data in //search_engine_data//AllMultipleSymptomsDF.csv
    Only do it everytime you update the data!
    """

    # Create csv filepath
    filepath = f"{__location__}\\search_engine_data\\multiple_symptoms\\"
    extension = "csv"

    # Search for all filenames
    all_filenames = [i for i in glob.glob(
        "{}*.{}".format(filepath, extension))]

    # Combine all csv file
    AllMultipleSymptomsDF = pd.concat(
        [pd.read_csv(f, index_col=0) for f in all_filenames], ignore_index=True)

    # Export it to concated csv
    AllMultipleSymptomsDF.to_csv(
        f"{__location__}\\search_engine_data\\AllMultipleSymptomsDF.csv")


def getMultipleSymptomsDataFrame(filename="AllMultipleSymptomsDF"):
    """Get DataFrame from csv. data of //search_engine_data//AllMultipleSymptomsDF.csv
    :param1: Just for testing, put a different filename for testing in here.
    :return: DataFrame with 376065 symptoms name and related URL
    """
    return pd.read_csv(f"{__location__}\\search_engine_data\\{filename}.csv", index_col=0)


def cleanCSVFromDuplicates(filename="AllMultipleSymptomsDF"):
    """Clean csv of //search_engine_data//{filename}.csv from duplicates
    :param1: Default filename is the one that should be cleaned standardly
    :return: cleaned dataframe; mainly for test purposes
    """
    # Read csv
    df = pd.read_csv(
        f"{__location__}\\search_engine_data\\{filename}.csv", index_col=0)

    # Remove duplicates
    AllMultipleSymptomsDFWithoutDuplicates = df.drop_duplicates(
        subset=["Symptom Name"], keep='first').reset_index(drop=True)

    # Export it to csv
    AllMultipleSymptomsDFWithoutDuplicates.to_csv(
        f"{__location__}\\search_engine_data\\AllMultipleSymptomsDF.csv")

    return AllMultipleSymptomsDFWithoutDuplicates


def deleteSinglePagesCSVs():
    """Deletes all single pages csv's that were created during getMultipleSymptomsFromOneSourcePageWEBMDSymptomsAZ() from 
    "healthcheckup\\search_engine_data\\multiple_symptoms\\multiple_symptoms_single_pages\\"
    """
    dir = __location__ + "\\search_engine_data\\multiple_symptoms\\multiple_symptoms_single_pages\\"
    filelist = glob.glob(os.path.join(dir, "*"))
    for f in filelist:
        os.remove(f)


def deleteMulitpleSymptomsCSVs():
    """Deletes all MultipleSymptoms pages csv's that were created during buildMultipleSymptomsCSV() from 
    "healthcheckup\\search_engine_data\\multiple_symptoms\\multiple_symptoms_single_pages\\"
    """
    dir = __location__ + "\\search_engine_data\\multiple_symptoms\\"
    filelist = glob.glob(os.path.join(dir, "*"))
    for f in filelist:
        os.remove(f)


def buildDataSet():
    """Get csv. data from all parent symptoms, all child symptoms, one csv file with SingleSymptoms and one with MultipleSymptoms
    Use this methode only to build the dataset or update dataset
    Can take up to 1,5 hours due to sleep timing otherwise requests get denied by the server
    """
    start = time.time()

    try:
        buildSingleSymptomsCSV()
    except Exception as e:
        print(f"Func: buildSingleSymptomsCSV | Error: {e}")

    try:
        buildMultipleSymptomsCSV()
    except Exception as e:
        print(f"Func: buildMultipleSymptomsCSV | Error: {e}")

    try:
        buildMergedSymptomDataCSV()
    except Exception as e:
        print(f"Func: buildMergedSymptomDataCSV | Error: {e}")

    try:
        cleanCSVFromDuplicates()
    except Exception as e:
        print(f"Func: cleanCSVFromDuplicates | Error: {e}")

    try:
        deleteSinglePagesCSVs()
    except Exception as e:
        print(f"Func: deleteSinglePagesCSVs | Error: {e}")
    
    try:
        deleteMulitpleSymptomsCSVs()
    except Exception as e:
        print(f"Func: deleteMulitpleSymptomsCSVs | Error: {e}")

    stop = time.time()
    print(f"buildDataSet: {stop-start}s")


########################################################
### TESTING AREA                                     ###
########################################################