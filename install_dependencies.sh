#!/bin/sh
pip3 install dash 
pip3 install dash-bootstrap-components
pip3 install pandas
pip3 install -U pytest
pip3 install "dash[testing]"
pip3 install chromedriver-py
pip3 install chromedriver-binary
pip3 install webdriver-manager
pip3 install selenium
