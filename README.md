# healthcheckup

## Try the app  
#### http://35.180.45.45:8050/

## Run the app

### On local
This option can not work if you dont have some tool such as python setup on your environement

1. Run: `./install_dependencies.sh`
2. Run: `python healthcheckup/gui.py`

### With Docker

1. Build: `docker build -t imagehealthcheckup:2.0 .`
    - Build to debug dockerfile:
    `docker build -t imagehealthcheckup:2.0 . --progress=plain --no-cache`

3. Tag the image: `docker tag imagehealthcheckup:2.0 imagehealthcheckupV2`

4. Run the image on local:
`docker run -p 8050:8050 imagehealthcheckup:2.0 `

5. The app is now available on localhost:8050 

## Test 

 1. Move inside the the subfolder `/healthcheckup` where the test folder is located. If you run the test command from another path the gui test will fail.
 2. Run `python -m pytest `

## Authors 

- M. Marco
- C. Gwenael 


## License
Not defined yet

